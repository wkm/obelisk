# 0.0.1 / 2013-05-18
* initial public release; a rough sketch of the concept
* gocircuit worker discovery through zookeeper
* counter and value metrics
* worker inspection through realtime stacktraces
* simplistic in-memory timeseries store
